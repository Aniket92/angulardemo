import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../core/services/product.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username;
  password;
  constructor(private route:Router,private service: ProductService) { }

  ngOnInit() {

  }
  login(){
    // this.service.login(this.username,this.password).subscribe((data:any)=>{
    //   debugger
    //   if(data.result == false){
    //     alert('Incorrect Username and password')
    //     return
    //   }
    //   else{
    //     localStorage.setItem('Token',data.data.token);
    //     this.route.navigate(['product/addProduct']);
    //   }
      
 
    // })
    this.route.navigate(['product/listing']);
  }
}
