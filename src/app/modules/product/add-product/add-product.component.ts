import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../core/services/product.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  addProduct: FormGroup;
  constructor(private formBuilder: FormBuilder,private http:HttpClient,private route:Router,private service: ProductService) { }

  ngOnInit() {
    this.initializeForm();
  }


  initializeForm(){
    this.addProduct=this.formBuilder.group({
        userId:['',Validators.required],
        title:['',[Validators.required, Validators.maxLength(40)]],
        body:['',[Validators.required, Validators.maxLength(100)]]
    });
  }

submit(){
  debugger;
    if (this.addProduct.invalid) {
            return;
        }
this.addProduct.value.id=11; 
var value=this.addProduct.value

this.http.post('https://jsonplaceholder.typicode.com/posts', value).subscribe(data => {
   this.route.navigate(['product/listing']);
   //alert('Data Saved');
});

//  this.service.get(value).subscribe((data:any)=>{
//     debugger
//  })

}


showProducts(){
   this.route.navigate(['product/listing']);
}
}
