import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatDialogConfig, MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from "@angular/router";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns = ['userId', 'body', 'title'];
  
  dataSource;// dataSource = ELEMENT_DATA;
   getResponse:any;
   name: number;
  constructor(
    private HttpClient:HttpClient,private route:Router) { }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.HttpClient.get(" https://jsonplaceholder.typicode.com/posts/10").subscribe((Data:any)=>{
debugger;
if (Array.isArray(Data)) {
this.getResponse = Data;
}
else{
  this.getResponse=[Data];
}
  this.dataSource = new MatTableDataSource(this.getResponse);
    });
  }



submit(){
  debugger
var no=this.name;
  this.HttpClient.get(" https://jsonplaceholder.typicode.com/posts/"+ no).subscribe((Data:any)=>{
debugger;
if (Array.isArray(Data)) {
this.getResponse = Data;
}
else{
  this.getResponse=[Data];
}
  this.dataSource = new MatTableDataSource(this.getResponse);
    });
}


allFind(){
  this.HttpClient.get(" https://jsonplaceholder.typicode.com/posts/").subscribe((Data:any)=>{
debugger;

this.getResponse = Data;

  this.dataSource = new MatTableDataSource(this.getResponse);
    });
}


addProduct(){
  this.route.navigate(['product/addProduct']);
}

}
