import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { MatTableModule, MatIconModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatToolbarModule } from '@angular/material' ;

import {MatSlideToggleModule, MatRadioModule,
   MatCheckboxModule, MatCardModule, MatProgressSpinnerModule, MatInputModule, MatButtonModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { LoginComponent } from './login/login.component';
import { ListingComponent } from './listing/listing.component';
import { AddProductComponent } from './add-product/add-product.component';

@NgModule({
  declarations: [ProductRoutingModule.components,  LoginComponent, ListingComponent, AddProductComponent],

  imports: [
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    CommonModule,
    ProductRoutingModule,
    MatSlideToggleModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatCardModule,
    MatToolbarModule
  ],
  providers: [
    MatDatepickerModule,
  ],
})
export class ProductModule { }
