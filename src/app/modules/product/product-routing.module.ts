import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import{ListingComponent} from'./listing/listing.component';
import{AddProductComponent} from './add-product/add-product.component'
import { from } from 'rxjs';
const routes: Routes = [
  
  
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Product Login' }
  },
  {
    path: 'listing',
    component: ListingComponent,
    data: { title: 'Product Listing' }
  },
  {
    path: 'addProduct',
    component: AddProductComponent,
    data: { title: 'Product Listing' }
  },

  


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
  static components = [
    ListingComponent
   
  ];
 }
