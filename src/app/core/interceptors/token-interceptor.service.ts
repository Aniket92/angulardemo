import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
    tokenData:any;
  constructor(private injector: Injector){
    this.tokenData = localStorage.getItem("Token")
  }
  intercept(req, next) {
    const authToken = localStorage.getItem("Token");
    let tokenizedReq = req.clone(
      {
        headers: req.headers.set('Authorization', "Bearer " + authToken)
      }
    )
    return next.handle(tokenizedReq)
  }

}