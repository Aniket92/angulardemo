import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }
  login(id,pass){
    return this.http.post('http://healthclixapi.azurewebsites.net/api/User/authenticate?UserName='+ id + '&' + 'Password=' + pass,"");
  }
  getAllProductCategories(){
    return this.http.get('http://healthclixapi.azurewebsites.net/api/Product/GetAllProductCategories');  
  }
  getAllProduct(){
    return this.http.get('http://healthclixapi.azurewebsites.net/api/Product/GetAllProduct');  
  }
  editProduct(body){
    return this.http.get('http://healthclixapi.azurewebsites.net/api/Product/GetProductById?ProductId='+ body);
  }
  addProduct(body){
    return this.http.post('  http://healthclixapi.azurewebsites.net/api/Product/AddEditProduct',body);
  }

  getAllProductAttribute(body){
    return this.http.get('http://healthclixapi.azurewebsites.net/api/ProductAttribute/GetAllProductAttributeANDProductNameByCategroryId?ProductCatId='+body);  
  }
  addProductAttribute(body){
    return this.http.post('http://healthclixapi.azurewebsites.net/api/ProductAttribute/AddEditProductAttribute',body);
  }
  getAllProductAttibute(){
    return this.http.get(' http://healthclixapi.azurewebsites.net/api/ProductAttribute/GetAllProductAttribute');  
  }
  editProductAttribute(productid:any){
    // return this.http.get(' http://healthclixapi.azurewebsites.net/api/ProductAttribute/GetProductAttributebyId?ProductId='+ productid '&' + 'AttributeId='+ productid);  
  }
  
};

  
