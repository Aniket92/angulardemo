import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'product', loadChildren: './modules/product/product.module#ProductModule' },
    { path: '', pathMatch: 'full', redirectTo: '/product/addProduct' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
